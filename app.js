require('dotenv').config();
require('./config').envCheck();
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const passport = require('passport');
require('./config/passport');
const cors = require('cors');
const helmet = require('helmet');
const routes = require('./routes');
const logger = require('./config/logger');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(helmet());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());
app.use(passport.initialize());
app.use('/api', routes);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  return res.status(404).json({ error: { message: 'Route Not Found' } })
});

// error handler
app.use((err, req, res, next) => {
  logger.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
  if (err instanceof Error) err = err.toString()
  console.error(err)
  res.status(err.status || 500);
  res.json({ error: { message: err.message || err.toString(), info: err } });
});


module.exports = app;
