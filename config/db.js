require("dotenv").config();
const mysql = require("mysql");
const { promisify } = require("util");

const db = mysql.createPool({
  connectionLimit: process.env.DB_CONNECTION_LIMIT,
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
});

// db.query = promisify(db.query);

db.query = promisify(db.query).bind(db);

const connection = () => {
  return new Promise((resolve, reject) => {
    db.getConnection((error, connection) => {
      if (error) reject(error);
      const query = (sql, binding) => {
        return new Promise((resolve, reject) => {
          connection.query(sql, binding, (error, result) => {
            if (error) reject(error);
            resolve(result);
          });
        });
      };
      const release = () => {
        return new Promise((resolve, reject) => {
          if (error) reject(error);
          resolve(connection.release());
        });
      };
      resolve({ query, release });
    });
  });
};

const CURRENT_TIMESTAMP = {
  toSqlString: function () {
    return "CURRENT_TIMESTAMP()";
  },
};

module.exports = { db, CURRENT_TIMESTAMP, connection };