const { db } = require('./db')

module.exports = {
  insertActivityLog: async (activity_initiator_id, old_ticket_value, changed_columns, comment) => {
    const insertActivityLogSql = "INSERT INTO activity_logs (activity_initiator_id, old_ticket_value, changed_columns, comment) VALUES (?, ?, ?, ?)";
    try {
      let results = await db.query(insertActivityLogSql, [activity_initiator_id, old_ticket_value, changed_columns, comment]);
      if (results.insertId) {
        return true;
      }
    } catch (error) {
      console.log('ERROR ACTIVITY LOG ', error);
    }
  }
}