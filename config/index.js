module.exports = {
  envCheck: () => {
    const arr = ["PORT", "NODE_ENV",  "DB_HOST", "DB_USER", "DB_PASSWORD", "DB_DATABASE", "DB_CONNECTION_LIMIT"];
    const missing = [];
    const check = arr.every((e) => {
      if (!!process.env[e]) return true;
      else {
        missing.push(e);
        return false;
      }
    });
    if (check) {
    } else {
      console.error({ message: "CHECK ENV VARIABLES", missing });
      process.exit(1);
    }
  },
};
