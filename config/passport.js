require('dotenv').config();
const passport = require('passport');
const { ExtractJwt: ExtractJWT, Strategy: JWTStrategy } = require('passport-jwt');
const { db } = require('../config/db');

// Strategy for all roles except Admin
passport.use('jwt', new JWTStrategy({
  jwtFromRequest: ExtractJWT.fromAuthHeaderWithScheme('Bearer'),
  secretOrKey: process.env.PASSPORT_SECRET
}, async (jwtPayload, cb) => {
  try {
    let findUserSql = `
      SELECT users.id as id, users.email as email, users.status as status, user_roles.name as role FROM users 
      JOIN user_roles ON 
      users.user_role_id = user_roles.id
      WHERE users.id = ?
    `;
    const user = (await db.query(findUserSql, [jwtPayload.id]))[0];
    if (!user) return cb(null, false);
    return cb(null, user);
  } catch (error) {
    console.error('ERROR IZ JWT STRATEGIJE', error)
    return cb(error, false);
  }
}));

// Strategy for Admin role
passport.use('admin', new JWTStrategy({
  jwtFromRequest: ExtractJWT.fromAuthHeaderWithScheme('Bearer'),
  secretOrKey: process.env.PASSPORT_SECRET
}, async (jwtPayload, cb) => {
  try {
    let findAdminSql = `
        SELECT users.id as id, users.email as email, users.status as status, user_roles.name as role FROM users 
        JOIN user_roles ON 
        users.user_role_id = user_roles.id
        WHERE users.id = ?
      `
    const admin = (await db.query(findAdminSql, [jwtPayload.id]))[0];
    if (!admin) return cb(null, false);
    if (admin.role === 'Admin') return cb(null, admin);
  } catch (error) {
    console.error('ERROR IZ ADMIN STRATEGIJE', error)
    return cb(error, false);
  }

}))