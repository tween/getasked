const express = require('express');
const router = express.Router();
const { aW, response } = require('../helpers');
const { isLoggedIn } = require('../middlewares');

// /api

router.get('/', aW((req, res) => {
  res.send('It works!');
}));

router.get('/me', isLoggedIn, aW(async (req, res) => {
  res.send(req.user);
}));


module.exports = router;