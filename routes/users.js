const express = require('express');
const router = express.Router();
const { db } = require('../config/db');
const { aW, response } = require('../helpers');

// /api/users

// GET ALL USERS EXCEPT ADMIN
router.get('/', aW(async (req, res) => {
  // where user role not admin
  const getUsersSql = "SELECT id, email FROM users WHERE user_role_id = ? AND status = ?";
  let users = await db.query(getUsersSql, [2, 'ACTIVE']);
  res.send(users);
}));

module.exports = router;
