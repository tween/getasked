const express = require('express');
const router = express.Router();
const { db, CURRENT_TIMESTAMP } = require('../config/db');
const { aW, response, findChangedIndexesinObjects } = require('../helpers');
const { isLoggedIn } = require('../middlewares');
const { insertActivityLog } = require('../config/activity-log');

// /api/tickets

router.use(isLoggedIn);

// GET ALL ACTIVE TICKETS
router.get('/status/:ticketStatus', aW(async (req, res) => {
  const { ticketStatus } = req.params;
  let getAllTicketsSql = `SELECT * FROM dashboard_tickets where is_deleted = ?;`;
  let getAllTicketsData = [false];
  if (ticketStatus !== 'all') {
    getAllTicketsSql += ' AND ticket_status_name = ?';
    getAllTicketsData.push(ticketStatus);
  }
  let tickets = await db.query(getAllTicketsSql, getAllTicketsData);
  res.send(tickets);
}))


// GET ALL AVAILABLE TICKET STATUS
router.get('/ticket-status', aW(async (req, res) => {
  const getTicketStatusSql = "SELECT id, name FROM ticket_status ORDER BY id ASC";
  let ticketStatus = await db.query(getTicketStatusSql);
  res.send(ticketStatus);
}));

// CREATE TICKET
router.post('/create-ticket', isLoggedIn, aW(async (req, res) => {
  const { assignedToId, title, content } = req.body;
  if (!title || !content) return res.status(200).send(response(false, 'All fields are mandatory to fill up'));
  const getPendingTicketStatusId = "SELECT id FROM ticket_status WHERE name = ?";
  let pendingTicketStatusId = (await db.query(getPendingTicketStatusId, ['PENDING']))[0].id;
  const createTicketSql = `INSERT INTO tickets (creator_id, ticket_status_id, assigned_to_id, title, content) VALUES (?, ?, ?, ?, ?)`;
  let assignedUserId = assignedToId ? assignedToId : null;
  let results = await db.query(createTicketSql, [req.user.id, pendingTicketStatusId, assignedUserId, title, content]);
  if (results.insertId) {
    await insertActivityLog(req.user.id, JSON.stringify(foundTicket), JSON.stringify({}), 'User Ticket Created');
    return res.send(response(true, 'Ticket has been created'));
  } else {
    return res.status(500).send(response(fale, 'Something went wrong', 'Please try again later'));
  }
}));

// GET TICKET BY ID
router.get('/show/:id', aW(async (req, res) => {
  const { id } = req.params;
  const getTicketByIdSql = `SELECT * FROM dashboard_tickets WHERE id = ? AND is_deleted = ?`;
  const foundTicket = (await db.query(getTicketByIdSql, [id, 0]))[0];
  if (!foundTicket) return res.send(response(false, "Ticket not found"));
  res.send(foundTicket);
}))

// EDIT TICKET BY ID, ALSO ADD ASSIGNEE TO THE TICKET
router.put('/edit/:id', aW(async (req, res) => {
  const { id } = req.params;
  const { assigned_to_id, ticket_status_id, title, content, ticket_status_name } = req.body;
  const getTicketByIdSql = "SELECT title, content, assigned_to_id, ticket_status_id, ticket_status_name, assignee_email FROM dashboard_tickets where is_deleted = ? AND id = ?;"
  const foundTicket = (await db.query(getTicketByIdSql, [false, id]))[0];
  if (!foundTicket) return res.send(response(false, "Ticket not found"));
  let changedValues = findChangedIndexesinObjects(foundTicket, req.body);
  let activityObject = {};
  changedValues.forEach(value => {
    activityObject[value] = `${foundTicket[value]} => ${req.body[value]}`
  })
  let assignedUserId = assigned_to_id ? assigned_to_id : foundTicket.assigned_to_id;
  let oldTicketStatusId = ticket_status_id ? ticket_status_id : foundTicket.ticket_status_id;
  const updateTicketSql = `UPDATE tickets SET ticket_status_id = ?, assigned_to_id = ?, title = ?, content = ?, updated_at = ? WHERE id = ?;`;
  let results = await db.query(updateTicketSql, [oldTicketStatusId, assignedUserId, title, content, CURRENT_TIMESTAMP, id]);
  if (results.affectedRows > 0) {
    await insertActivityLog(req.user.id, JSON.stringify(foundTicket), JSON.stringify(activityObject), 'User Ticket Update');
    return res.send(response(true, 'Ticket has been succesfully updated'));
  } else {
    return res.send(response(false, 'Something went wrong!', 'Plese, try again later'));
  }
}));

// SOFT DELETE TICKET BY ID
router.delete('/delete/:id', aW(async (req, res) => {
  const { id } = req.params;
  const getTicketByIdSql = "SELECT * FROM tickets WHERE id = ?";
  const foundTicket = (await db.query(getTicketByIdSql, [id]))[0];
  if (!foundTicket) return res.send(response(false, "Ticket not found"));
  if (foundTicket.creator_id !== req.user.id) return res.status(403).send(response(false, "You don't have privileges to do that"));
  const deleteFoundTicketSql = "UPDATE tickets SET is_deleted = ?, updated_at = ? WHERE id = ?";
  let results = await db.query(deleteFoundTicketSql, [true, CURRENT_TIMESTAMP, id]);
  if (results.affectedRows > 0) {
    await insertActivityLog(req.user.id, JSON.stringify(foundTicket), JSON.stringify({ is_deleted: "Active => Archived" }), 'User Ticket Delete');
    res.send(response(true, 'Ticket has been succesfully deleted'));
  } else {
    res.send(response(false, 'Something went wrong!', 'Plese, try again later'));
  }
}));

module.exports = router;