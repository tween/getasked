const express = require('express');
const router = express.Router();
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');
const { db, CURRENT_TIMESTAMP } = require('../config/db');
const { aW, response, generateUserConfirmationCode } = require('../helpers');

// /api/auth

// CREATE NEW USER
router.post('/register', aW(async (req, res) => {
  const { email, password, passwordConfirmation } = req.body;
  if (!email || !password || !passwordConfirmation) return res.send(response(false, 'You have to fill all fields'));
  if (password !== passwordConfirmation) return res.send(response(false, 'Passwords must match'));
  const getRoleIdSql = "SELECT id FROM user_roles WHERE name = ?";
  const roleId = (await db.query(getRoleIdSql, 'User'))[0].id;
  const salt = bcrypt.genSaltSync(10);
  let hashedPassword = bcrypt.hashSync(password, salt);
  let userConfirmationCode = generateUserConfirmationCode();
  const checkForExistingUserByEmailSql = "SELECT id, email FROM users WHERE email = ?";
  let foundUser = (await db.query(checkForExistingUserByEmailSql, [email]))[0];
  if (foundUser) return res.status(200).send(response(false, 'Email has been registered to another user'));
  const createUserSql = "INSERT INTO users (user_role_id, email, password, user_confirmation_code) VALUES (?, ?, ?, ?)";
  let results = await db.query(createUserSql, [roleId, email, hashedPassword, userConfirmationCode]);
  if (results.insertId) {
    // mailer??
    return res.send(response(true, 'You have been registered. Please, check your email to activate your profile.'));
  } else {
    return res.status(500).send(response(fale, 'Something went wrong', 'Please try again later'));
  }
}));

// ACTIVATE USER PROFILE
router.put('/activate', aW(async (req, res) => {
  const { code } = req.body;
  if (!code) return res.send(response(false, 'Please check your confirmation code'));
  const findUserByConfirmationCodeSql = "SELECT * FROM users WHERE user_confirmation_code = ?";
  let user = (await db.query(findUserByConfirmationCodeSql, [code]))[0];
  if (!user) return res.send(response(false, 'User not found!'));
  const updateUserStatusSql = "UPDATE users SET user_confirmation_code = ?, status = ?, updated_at = ? WHERE email = ?";
  let results = await db.query(updateUserStatusSql, [null, 'ACTIVE', CURRENT_TIMESTAMP, user.email]);
  if (results.affectedRows > 0) {
    res.send(response(true, 'Your account has been activated'));
  } else {
    res.send(response(false, 'Something went wrong!', 'Plese, try again later'));
  }
}));

// LOGIN
router.post('/login', aW(async (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) return res.send(response(false, 'You have to fill all fields'));
  let findUserByEmailSql = `
    SELECT users.id as id, users.email as email, users.password as password, users.status as status, user_roles.name as role FROM users 
    JOIN user_roles ON users.user_role_id = user_roles.id
    WHERE users.email = ?
`;
  let user = (await db.query(findUserByEmailSql, [email]))[0];
  if (!user) return res.status(404).send(response(false, 'Please check your login credentials'));
  if (bcrypt.compareSync(password, user.password)) {
    if (user.status === 'ACTIVE') {
      const whatToSign = { id: user.id, role: user.role_name };
      const token = jwt.sign(whatToSign, process.env.PASSPORT_SECRET, { expiresIn: '1d' });
      return res.status(200).send({
        token: 'Bearer ' + token,
        user: {
          id: user.id,
          email: user.email,
          status: user.status,
          role: user.role
        }
      })
    } else {
      return res.send(response(false, "You didn't activate your account. Go to your email to do so."));
    }
  } else {
    return res.send(response(false, 'User not found!'));
  }
}));

module.exports = router;