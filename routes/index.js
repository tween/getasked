const express = require('express');
const router = express.Router();


// ADMIN
router.use('/admin', require('./admin'));

// AUTH
router.use('/auth', require('./auth'));

router.use('/', require('./home'));
router.use('/tickets', require('./tickets'));
router.use('/users', require('./users'));

module.exports = router;
