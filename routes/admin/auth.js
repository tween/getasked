const express = require('express');
const router = express.Router();
const { aW, response } = require('../../helpers');
const { db } = require('../../config/db');
const { isLoggedInAdmin } = require('../../middlewares');

// /admin/auth

router.get('/', isLoggedInAdmin, aW(async (req, res) => {
  res.send('ADMIN AUTH')
}));

module.exports = router;