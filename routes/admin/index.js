const express = require('express');
const router = express.Router();

router.use('/activity-logs', require('./activity-logs'));
router.use('/auth', require('./auth'));
router.use('/tickets', require('./tickets'));
router.use('/users', require('./users'));

module.exports = router;