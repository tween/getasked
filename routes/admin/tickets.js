const express = require('express');
const router = express.Router();
const { aW, response } = require('../../helpers');
const { db, CURRENT_TIMESTAMP } = require('../../config/db');
const { isLoggedInAdmin } = require('../../middlewares');
const { findChangedIndexesinObjects } = require('../../helpers');
const { insertActivityLog } = require('../../config/activity-log');

// /admin/tickets

router.use(isLoggedInAdmin);

// GET ALL TICKETS, ARCHIVED ALSO
router.get('/', aW(async (req, res) => {
  const getAllTicketsSql = `SELECT * FROM dashboard_tickets`;
  const allTickets = await db.query(getAllTicketsSql);
  res.send(allTickets);
}))

router.get('/single-ticket/:id', aW(async (req, res) => {
  const { id } = req.params;
  const getTicketByIdSql = `SELECT * FROM dashboard_tickets WHERE id = ?`;
  const foundTicket = (await db.query(getTicketByIdSql, [id]))[0];
  if (!foundTicket) return res.send(response(false, "Ticket not found"));
  res.send(foundTicket);
}))

// UPDATE TICKET
router.put('/:id', aW(async (req, res) => {
  const { id } = req.params;
  const { assigned_to_id, ticket_status_id, title, content, ticket_status_name } = req.body;
  const getTicketByIdSql = "SELECT title, content, assigned_to_id, ticket_status_id, ticket_status_name, assignee_email FROM dashboard_tickets where id = ?;"
  const foundTicket = (await db.query(getTicketByIdSql, [id]))[0];
  if (!foundTicket) return res.send(response(false, "Ticket not found"));
  let changedValues = findChangedIndexesinObjects(foundTicket, req.body);
  let activityObject = {};
  changedValues.forEach(value => {
    activityObject[value] = `${foundTicket[value]} => ${req.body[value]}`
  })
  let assignedUserId = assigned_to_id ? assigned_to_id : foundTicket.assigned_to_id;
  let oldTicketStatusId = ticket_status_id ? ticket_status_id : foundTicket.ticket_status_id;
  const updateTicketSql = `UPDATE tickets SET ticket_status_id = ?, assigned_to_id = ?, title = ?, content = ?, updated_at = ? WHERE id = ?;`;
  if (Object.keys(activityObject).length > 0) {
    let results = await db.query(updateTicketSql, [oldTicketStatusId, assignedUserId, title, content, CURRENT_TIMESTAMP, id]);
    if (results.affectedRows > 0) {
      await insertActivityLog(req.user.id, JSON.stringify(foundTicket), JSON.stringify(activityObject), 'Admin Ticket Update');
      return res.send(response(true, 'Ticket has been succesfully updated'));
    } else {
      return res.send(response(false, 'Something went wrong!', 'Plese, try again later'));
    }
  } else {
    return res.send(response(false, 'Nothing to update'))
  }
}));

// REACTIVATE TICKET
router.put('/activate/:id', aW(async (req, res) => {
  const { id } = req.params;
  const getTicketByIdSql = `SELECT * FROM dashboard_tickets WHERE id = ?`;
  const foundTicket = (await db.query(getTicketByIdSql, [id]))[0];
  if (!foundTicket) return res.send(response(false, "Ticket not found"));
  if (!foundTicket.is_deleted) return res.send(response(false, 'Ticket has been already activated'));
  const activateTicketSql = `UPDATE tickets SET is_deleted = ? WHERE id = ?`;
  let results = await db.query(activateTicketSql, [0, id]);
  if (results.affectedRows > 0) {
    await insertActivityLog(req.user.id, JSON.stringify(foundTicket), JSON.stringify({ is_deleted: "Archived => Active" }), 'Admin Ticket Reactivation');
    return res.send(response(true, 'Ticket has been succesfully reactivated'));
  } else {
    return res.send(response(false, 'Something went wrong!', 'Plese, try again later'));
  }
}))

// ARCHIVE TICKET
router.delete('/soft-delete/:id', aW(async (req, res) => {
  const { id } = req.params;
  const getTicketByIdSql = `SELECT * FROM dashboard_tickets WHERE id = ?`;
  const foundTicket = (await db.query(getTicketByIdSql, [id]))[0];
  if (!foundTicket) return res.send(response(false, "Ticket not found"));
  if (foundTicket.is_deleted) return res.send(response(false, 'Ticket has been already archived'));
  const activateTicketSql = `UPDATE tickets SET is_deleted = ? WHERE id = ?`;
  let results = await db.query(activateTicketSql, [1, id]);
  if (results.affectedRows > 0) {
    await insertActivityLog(req.user.id, JSON.stringify(foundTicket), JSON.stringify({ is_deleted: "Active => Archive" }), 'Admin Ticket Archived');
    return res.send(response(true, 'Ticket has been succesfully archived'));
  } else {
    return res.send(response(false, 'Something went wrong!', 'Plese, try again later'));
  }
}));

// HARD DELETE TICKET
router.delete('/hard-delete/:id', aW(async (req, res) => {
  const { id } = req.params;
  const getTicketByIdSql = `SELECT * FROM dashboard_tickets WHERE id = ?`;
  const foundTicket = (await db.query(getTicketByIdSql, [id]))[0];
  if (!foundTicket) return res.send(response(false, "Ticket not found"));
  const activateTicketSql = `DELETE FROM tickets WHERE id = ?`;
  let results = await db.query(activateTicketSql, [id]);
  if (results.affectedRows > 0) {
    await insertActivityLog(req.user.id, JSON.stringify(foundTicket), JSON.stringify({ is_deleted: "PERMANENT" }), 'Admin Ticket Hard Delete');
    return res.send(response(true, 'Ticket has been succesfully archived'));
  } else {
    return res.send(response(false, 'Something went wrong!', 'Plese, try again later'));
  }
}));

// GET TICKET STATUS WITH CREATOR EMAIL
router.get('/ticket-status', aW(async (req, res) => {
  const getTicketStatusSql = "SELECT ticket_status.*, users.email as creator_email FROM ticket_status LEFT JOIN users ON ticket_status.creator_id = users.id;"
  const allTicketStatus = await db.query(getTicketStatusSql);
  res.send(allTicketStatus);
}))

// CREATE TICKET STATUS NAME
router.post('/create-status', aW(async (req, res) => {
  const { name } = req.body;
  if (!name) return res.status(200).send(response(false, 'Status name is mandatory'));
  const createTicketStatusSql = "INSERT INTO ticket_status (name, creator_id) VALUES (?, ?)";
  let results = await db.query(createTicketStatusSql, [name, req.user.id]);
  if (results.insertId) {
    return res.send(response(true, `Ticket Status ${name} has been created`))
  } else {
    return res.status(500).send(response(false, 'Something went wrong', 'Please try again later'));
  }
}));

// UPDATE STATUS NAME
router.put('/update-status/:id', aW(async (req, res) => {
  const { id } = req.params;
  const { name } = req.body;
  const findTicketStatusByIdSql = "SELECT id, name FROM ticket_status WHERE id = ?";
  const foundTicketStatus = (await db.query(findTicketStatusByIdSql, [id]))[0];
  if (!foundTicketStatus) return res.status(200).send(response(false, 'Ticket Status not found'));
  const updateTicketStatusSql = "UPDATE ticket_status SET name = ? WHERE id = ?";
  let results = await db.query(updateTicketStatusSql, [name, id]);
  if (results.affectedRows > 0) {
    return res.send(response(true, `Ticket Status ${foundTicketStatus.name} has been succesfully renamed to ${name}`));
  } else {
    return res.send(response(false, 'Something went wrong!', 'Plese, try again later'));
  }
}))

// DELETE TICKET STATUS BY => CALLS BEFORE DELETE TRIGGER
router.delete('/delete/ticket-status/:id', aW(async (req, res) => {
  const { id } = req.params;
  const findTicketStatusByIdSql = "SELECT id, name FROM ticket_status WHERE id = ?";
  const foundTicketStatus = (await db.query(findTicketStatusByIdSql, [id]))[0];
  if (!foundTicketStatus) return res.status(200).send(response(false, 'Ticket Status not found'));
  const deleteTicketStatusSql = "DELETE FROM ticket_status WHERE id = ?";
  let results = await db.query(deleteTicketStatusSql, [id]);
  if (results.affectedRows > 0) {
    return res.send(response(true, `Ticket Status ${foundTicketStatus.name} has been deleted`))
  } else {
    return res.status(500).send(response(false, 'Something went wrong', 'Please try again later'));
  }
}));

module.exports = router;