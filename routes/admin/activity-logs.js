const express = require('express');
const router = express.Router();
const { aW, response } = require('../../helpers');
const { db, CURRENT_TIMESTAMP } = require('../../config/db');
const { isLoggedInAdmin } = require('../../middlewares');
const { findChangedIndexesinObjects } = require('../../helpers');
const { insertActivityLog } = require('../../config/activity-log');

// /admin/activity-logs

// router.use(isLoggedIn);

router.get('/', aW(async (req, res) => {
  const getActivityLogsSql = `SELECT activity_logs.*, users.email as initiator_email FROM activity_logs JOIN users ON activity_logs.activity_initiator_id = users.id;`
  const allActivityLogs = await db.query(getActivityLogsSql);
  res.send(allActivityLogs);
}))

router.get('/single-log/:id', aW(async (req, res) => {
  const { id } = req.params;
  const getSingleActivityLogSql = `SELECT activity_logs.*, users.email as initiator_email FROM activity_logs JOIN users ON activity_logs.activity_initiator_id = users.id WHERE activity_logs.id = ?;`
  const singleLog = (await db.query(getSingleActivityLogSql, [id]))[0];
  res.send(singleLog);
}))

module.exports = router;