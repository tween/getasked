const express = require('express');
const router = express.Router();
const { aW, response } = require('../../helpers');
const { db } = require('../../config/db');
const { isLoggedInAdmin } = require('../../middlewares');

// /admin/users

// GET ALL USERS
router.get('/', aW(async (req, res) => {
  const getAllUsersSql = `
    SELECT users.id as id, users.email as email, users.status as status, user_roles.name as role, users.created_at, users.updated_at FROM users 
    JOIN user_roles ON users.user_role_id = user_roles.id;
  `;
  const users = await db.query(getAllUsersSql);
  res.send(users);
}));

// CHANGE STATUS OF USER
router.put('/:id', aW(async (req, res) => {
  const { id } = req.params;
  if (!id) return res.status(404).send(response(false, 'User ID is mandatory'));
  const findUserByIdSql = "SELECT id, email, status FROM users WHERE id = ?";
  const foundUser = (await db.query(findUserByIdSql, [id]))[0];
  if (!foundUser) return res.status(404).send(response(false, 'User not found'));
  if (foundUser.status === 'ACTIVE') return res.send(response(false, 'User has been already activated'));
  const updateUserStatusSql = `UPDATE users SET status = ?, user_confirmation_code = ? WHERE id = ?`;
  let results = await db.query(updateUserStatusSql, ['ACTIVE', null, id]);
  if (results.affectedRows > 0) {
    return res.send(response(true, `User ${foundUser.email} has been activated!`));
  } else {
    return res.status(500).send(response(false, 'Something went wrong', 'Please, try again later'));
  }
}));

// ON DELETE USER, HIS TICKETS WILL REMAIN. CREATOR_ID WILL BE NULL, ALSO, ASSIGNMENT TICKETS WILL BE NULL FIELD.
router.delete('/:id', aW(async (req, res) => {
  if (!id) return res.status(404).send(response(false, 'User ID is mandatory'));
  const findUserByIdSql = "SELECT id, email, status FROM users WHERE id = ?";
  const foundUser = (await db.query(findUserByIdSql, [id]))[0];
  if (!foundUser) return res.status(404).send(response(false, 'User not found'));
  const deleteUserByIdSql = "DELETE FROM users WHERE id = ?";
  let results = await db.query(deleteUserByIdSql, [id]);
  if (results.affectedRows > 0) {
    return res.send(response(true, `User ${foundUser.email} has been deleted!`));
  } else {
    return res.status(500).send(response(false, 'Something went wrong', 'Please, try again later'));
  }
}));



module.exports = router;