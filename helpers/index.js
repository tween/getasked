require("dotenv").config();
const crypto = require("crypto");

module.exports = {
  aW: (fn) => (req, res, next) => Promise.resolve(fn(req, res, next)).catch(next),
  response: (bool, message, info) => {
    if (bool) {
      return { success: { message, info } }
    } else {
      return { error: { message, info } }
    }
  },
  generateUserConfirmationCode: () => {
    return crypto .createHash("md5").update(Date.now() + process.env.USER_CONFIRMATION_CODE_SECRET).digest("hex");
  },
  findChangedIndexesinObjects: (primeObject, compareObject) => {
    let changedValues = [];
    Object.keys(primeObject).forEach(key => {
      if (primeObject[key] !== compareObject[key]) {
        changedValues.push(key);
      }
    })
    return changedValues;
  }
}