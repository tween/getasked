const passport = require('passport');
const jwt = require('jsonwebtoken');
const { response } = require('../helpers');

module.exports = {
  isLoggedIn: (req, res, next) => passport.authenticate('jwt', { session: false }, (error, user, info) => {
    if (error) return res.status(401).json(response(false, 'Unauthorized', error))
    if (!user) return res.status(401).json(response(false, 'Unauthorized', 'Invalid token or user not found'))
    req.login(user, { session: false }, (error) => {
      if (error) return res.status(401).json(response(false, 'Unauthorized', error))
      return next()
    })
  })(req, res, next),
  isLoggedInAdmin: (req, res, next) => passport.authenticate('admin', { session: false }, (error, admin, info) => {
    if (error) return res.status(401).json(response(false, 'Unauthorized', error))
    if (!admin) return res.status(401).json(response(false, 'Unauthorized', 'Invalid token or admin not found'))
    req.login(admin, { session: false }, (error) => {
      if (error) return res.status(401).json(response(false, 'Unauthorized', error))
      return next()
    })
  })(req, res, next)
};
