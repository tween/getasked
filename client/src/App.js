import "./App.css";
import React, { useEffect } from 'react';
import Navigation from "./components/Navigation";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router } from "react-router-dom";
import Routes from "./Routes";
import LoadAxios from "./hoc/loadAxios";

function App() {
  return (
    <div>
      <LoadAxios />
      <Router>
        <Navigation />
        <Routes />
      </Router>
    </div>
  );
}

export default App;
