import React from 'react'
import { Card, Form, Col } from "react-bootstrap";
import { Link } from 'react-router-dom';
import useHelper from '../hooks/useHelper';


function SingleTicket(props) {
  const { id, title, content, assignee_email, created_at, updated_at } = props;
  const { formatDate } = useHelper();
  return (
    <Card style={{ width: '20rem' }}>
      <Card.Header>{title}</Card.Header>
      <Card.Body>
        <Card.Subtitle className="mb-2 text-muted">Assigned To</Card.Subtitle>
        <Card.Text>{assignee_email || 'Not Assigned'}</Card.Text>
        <Card.Text>
          {content}
        </Card.Text>
        <Card.Text>
          <Form.Row>
            <Form.Group>
              <Form.Label>Created At</Form.Label>
              <Form.Control type="text" value={formatDate(created_at)} readOnly />
            </Form.Group>
          </Form.Row>
        </Card.Text>
        <Link to={"/tickets/show/" + id}>Other details</Link>
      </Card.Body>
    </Card>
  )
}

export default SingleTicket
