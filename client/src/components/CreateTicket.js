import React, { useState, useEffect } from 'react'
import { Container, Row, Col, Form, Button, Toast } from "react-bootstrap";
import { createTicket } from '../services/ticket.service';
import { getUsers } from '../services/user.service';
import RenderError from './RenderError';


function CreateTicket() {
  const [inputs, setInputs] = useState({
    title: '',
    content: '',
    assignedToId: ''
  });
  const { title, content, assignedToId } = inputs;
  const [users, setUsers] = useState([]);
  const [errors, setErrors] = useState({});
  const [response, setResponse] = useState({
    success: '',
    error: ''
  });

  function validate() {
    let error = false;
    let fields = Object.keys(inputs).slice(0, 2);
    fields.forEach(field => {
      let displayField = field.replaceAll('_', ' ');
      displayField = displayField.charAt(0).toUpperCase() + displayField.slice(1) + ' is required';
      if (inputs[field] === "") {
        setErrors(errors => ({ ...errors, [field]: displayField }))
      } else {
        setErrors(errors => ({ ...errors, [field]: '' }));
      }
      error = true;
    })
    return error;
  }

  function handleChange(e) {
    const { name, value } = e.target;
    setInputs(inputs => ({ ...inputs, [name]: value }));
  }

  useEffect(async () => {
    let fetchedUsers = await getUsers();
    setUsers(fetchedUsers);
  }, []);

  async function handleCreateTicketForm(e) {
    e.preventDefault();
    const error = validate();
    if (!!error) {
      let createResponse = await createTicket(inputs);
      if (createResponse.error) {
        setResponse(response => ({ ...response, error: createResponse.error.message }))
      } else {
        setResponse(response => ({ ...response, success: createResponse.success.message }))
      }
      Object.keys(inputs).map(key => {
        setInputs(inputs => ({ ...inputs, [key]: '' }));
      })
    }
  }

  return (
    <Container>
      <Row>
        <Col>
          <p style={response.error ? { color: "red" } : { color: "green" }}>{response.success || response.error}</p>
        </Col>
      </Row>
      <Row>
        <Col className="form-header">
          Create Ticket
        </Col>
      </Row>
      <Row>
        <Col>
          <Form className="form" >
            <Form.Group controlId="ticket-title">
              <Form.Label>Ticket title</Form.Label>
              <Form.Control name="title" onChange={handleChange} type="text" value={title} placeholder="Enter Ticket title" />
              <RenderError errors={errors} name="title" />
            </Form.Group>
            <Form.Group controlId="ticket-content">
              <Form.Label>Ticket content</Form.Label>
              <Form.Control as="textarea" rows={5} name="content" value={content} onChange={handleChange} />
              <RenderError errors={errors} name="content" />
            </Form.Group>
            <Form.Group controlId="ticket-assign-to">
              <Form.Label>Assign Ticket to</Form.Label>
              <Form.Control as="select" name="assignedToId" onChange={handleChange}>
                <option value={assignedToId}>Not Assigned</option>
                {users && users.map(({ id, email }) => {
                  return (<option value={id} key={id}>{email}</option>)
                })}
              </Form.Control>
            </Form.Group>
            <Button variant="primary" onClick={handleCreateTicketForm}>
              Create Ticket
            </Button>
          </Form>
        </Col>
      </Row>
    </Container >
  )
}

export default CreateTicket
