import React, { Fragment, useState, useEffect } from "react";
import { Form, Table, Row, Col, Button, Container } from 'react-bootstrap';
import { Link } from "react-router-dom";
import useHelper from "../../hooks/useHelper";
import { getActivityLogs } from "../../services/activity-log.service";


function AdminActivity() {
  const [logs, setLogs] = useState([]);
  const { formatDate } = useHelper();
  useEffect(async () => {
    let fetchedLogs = await getActivityLogs();
    setLogs(fetchedLogs);
  }, [])


  return (
    <Fragment>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Initiator ID</th>
            <th>Initiator Email</th>
            <th>Old Ticket Values</th>
            <th>Changed Columns</th>
            <th>Comment</th>
            <th>Created At</th>
          </tr>
        </thead>
        <tbody>
          {logs && logs.map(log => {
            return <tr key={log.id}>
              <td><Link to={"/admin/activity/show/" + log.id}>{log.id}</Link></td>
              <td>{log.activity_initiator_id}</td>
              <td>{log.initiator_email}</td>
              <td>{log.old_ticket_value.slice(0, 100) + "..."}</td>
              <td>{log.changed_columns.slice(0, 100) + "..."}</td>
              <td>{log.comment.slice(0, 100) + "..."}</td>
              <td>{formatDate(log.created_at)}</td>
            </tr>
          })}
        </tbody>
      </Table>
    </Fragment>
  );
}

export default AdminActivity;
