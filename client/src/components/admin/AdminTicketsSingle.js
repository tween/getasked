import React, { useState, useEffect } from 'react'
import { Container, Row, Col, Form, Button, Modal } from "react-bootstrap";
import useHelper from '../../hooks/useHelper';
import { getUsers } from '../../services/user.service';
import InfoMessageHeader from '../InfoMessageHeader';
import { deleteTicket, getTicketStatus, updateTicket } from '../../services/ticket.service';
import { hardDeleteTicket, getSingleTicket, updateAdminTicket, makeTicketActive, softDeleteTicket } from '../../services/admin.service';
import { useHistory } from 'react-router-dom';


function AdminTicketsSingle(props) {
  const [ticket, setTicket] = useState({});
  const [ticketStatus, setTicketStatus] = useState([]);
  const [users, setUsers] = useState([]);
  const [inputs, setInputs] = useState({
    title: '',
    content: '',
    ticket_status_id: '',
    assigned_to_id: ''
  });
  const [response, setResponse] = useState({
    success: '',
    error: ''
  });
  const [needUpdate, setNeedUpdate] = useState(false);
  const { formatDate } = useHelper();
  const { id } = props.match.params;
  const history = useHistory();

  useEffect(async () => {
    let singleTicket = await getSingleTicket(id);
    setTicket(singleTicket);
    setInputs(inputs => ({ ...inputs, ...singleTicket }));
    let fetchedTicketStatus = await getTicketStatus();
    setTicketStatus(fetchedTicketStatus);
    let fetchedUsers = await getUsers();
    setUsers(fetchedUsers);
  }, [needUpdate])

  const deleteStatus = [
    { id: 1, name: 'Active', is_deleted: 0 },
    { id: 2, name: 'Archived', is_deleted: 1 },
  ]

  function handleChange(e) {
    const { name, value } = e.target;
    setInputs(inputs => ({ ...inputs, [name]: value }));
  }

  async function handleSoftDelete() {
    handleClose();
    let softDeleteResponse = await softDeleteTicket(id);
    if (softDeleteResponse.error) {
      setResponse(response => ({ ...response, error: softDeleteResponse.error.message }))
    } else {
      setResponse(response => ({ ...response, success: softDeleteResponse.success.message }))
    }
    setNeedUpdate(!needUpdate);
  }

  async function handleHardDelete() {
    handleClose();
    let hardDeleteResponse = await hardDeleteTicket(id);
    if (hardDeleteResponse.error) {
      setResponse(response => ({ ...response, error: hardDeleteResponse.error.message }))
    } else {
      history.push('/admin/tickets');
    }
  }

  async function activateTicket() {
    let reactivateResponse = await makeTicketActive(id);
    if (reactivateResponse.error) {
      setResponse(response => ({ ...response, error: reactivateResponse.error.message }))
    } else {
      setResponse(response => ({ ...response, success: reactivateResponse.success.message }))
    }
    setNeedUpdate(!needUpdate);
  }

  // MODAL
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  function ActionModal() {
    return (
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Delete Actions</Modal.Title>
        </Modal.Header>
        <Modal.Body>You are about to delete Ticket: {ticket.title}</Modal.Body>
        <Modal.Footer>
          <Button variant="info" disabled={ticket.is_deleted ? true : false} onClick={handleSoftDelete}>
            Archive - Soft Delete
            </Button>
          <Button variant="danger" onClick={handleHardDelete}>
            Permanent Delete - Hard Delete
            </Button>
        </Modal.Footer>
      </Modal>
    )
  }

  async function handleEdit() {
    const { title, content, assigned_to_id, ticket_status_id } = inputs;
    let matchStatusFindId = ticket_status_id ? ticket_status_id : ticket.ticket_status_id
    const ticket_status_name = ticketStatus.find(status => status.id == matchStatusFindId).name;
    let matchUserFindId = assigned_to_id ? assigned_to_id : ticket.assigned_to_id;
    const assigned_to_email = users.find(user => user.id == matchUserFindId);
    let updateResponse = await updateAdminTicket(id, { title, content, assigned_to_id, ticket_status_id, ticket_status_name, assignee_email: assigned_to_email.email });
    if (updateResponse.error) {
      setResponse(response => ({ ...response, error: updateResponse.error.message }))
    } else {
      setResponse(response => ({ ...response, success: updateResponse.success.message }))
    }
    setNeedUpdate(!needUpdate);
  }

  return (
    <Container>
      <InfoMessageHeader response={response} />
      <Row>
        <Col className="form-header">
          Ticket Details
        </Col>
      </Row>
      <Row>
        <Col>
          <Form className="form">

            <Form.Group controlId="ticket-title">
              <Form.Label>Ticket title No. {id}</Form.Label>
              <Form.Control name="title" type="text" placeholder="Enter Ticket title" value={inputs.title} onChange={handleChange} />
            </Form.Group>
            <Form.Group controlId="ticket-content">
              <Form.Label>Ticket content</Form.Label>
              <Form.Control as="textarea" rows={5} name="content" value={inputs.content} onChange={handleChange} />
            </Form.Group>

            <Form.Group controlId="ticket-status">
              <Form.Label>Ticket Status</Form.Label>
              <Form.Control as="select" name="ticket_status_id" defaultValue={ticket.ticket_status_id} onChange={handleChange}>
                {ticketStatus && ticketStatus.map(({ id, name }) => {
                  return (
                    <option selected={ticket.ticket_status_id === id ? true : false} value={id} key={id}>{name}</option>
                  )
                })}
              </Form.Control>
            </Form.Group>

            <Form.Group controlId="ticket-assign-to">
              <Form.Label>Assign Ticket to</Form.Label>
              <Form.Control as="select" name="assigned_to_id" defaultValue={ticket.assigned_to_id} onChange={handleChange} >
                <option selected={ticket.assigned_to_id === null ? true : false} value={null}>Not Assigned</option>
                {users && users.map(({ id, email }) => {
                  return (
                    <option selected={ticket.assigned_to_id === id ? true : false} value={id} key={id}>{email}</option>
                  )
                })}

              </Form.Control>
            </Form.Group>
            <Form.Group controlId="ticket-is-deleted">
              <Form.Label>Is Deleted</Form.Label>
              <Form.Control as="select" name="is_deleted" defaultValue={ticket.is_deleted} readOnly >
                {deleteStatus && deleteStatus.map(({ id, name, is_deleted }) => {
                  return (
                    <option selected={ticket.is_deleted === is_deleted ? true : false} value={id} key={id}>{name}</option>
                  )
                })}
              </Form.Control>
            </Form.Group>

            <Form.Row>
              <Form.Group as={Col} controlId="ticket-created-at">
                <Form.Label>Created At</Form.Label>
                <Form.Control placeholder={formatDate(ticket.created_at)} readOnly />
              </Form.Group>

              <Form.Group as={Col} controlId="ticket-updated-at">
                <Form.Label>Updated At</Form.Label>
                <Form.Control placeholder={formatDate(ticket.updated_at)} readOnly />
              </Form.Group>
            </Form.Row>
            <Form.Group controlId="ticket-creator">
              <Form.Label>Ticket created by</Form.Label>
              <Form.Control name="ticket-creator" placeholder={ticket.creator_email} readOnly />
            </Form.Group>
            <Row>
              <Col>
                <Button variant="primary" onClick={handleEdit}>Edit Ticket</Button>
              </Col>
              <Col>
                <Button variant="danger" onClick={handleShow}>Delete Ticket</Button>
              </Col>
              <Col>
                <Button variant="info" disabled={!ticket.is_deleted ? true : false} onClick={activateTicket}>Active Ticket</Button>
              </Col>
            </Row>
          </Form>
        </Col>
      </Row>
      <ActionModal />
    </Container >
  )
}

export default AdminTicketsSingle
