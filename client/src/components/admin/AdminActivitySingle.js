import React, { useState, useEffect } from 'react'
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import useHelper from '../../hooks/useHelper';
import { getSingleActivityLog } from '../../services/activity-log.service';


function AdminActivitySingle(props) {
  const [log, setLog] = useState({});
  const { id } = props.match.params;
  const { formatDate } = useHelper();

  useEffect(async () => {
    let fetchedLog = await getSingleActivityLog(id);
    setLog(fetchedLog);
  }, [])

  return (
    <Container>
      <Row>
        <Col className="form-header">
          Single Activity Log
        </Col>
      </Row>
      <Row>
        <Col>
          <Form.Group controlId="initiator-email">
            <Form.Label>Initiator Email</Form.Label>
            <Form.Control name="initiator_email" type="text" placeholder={log.initiator_email} />
          </Form.Group>
          <Form.Group controlId="comment">
            <Form.Label>Comment</Form.Label>
            <Form.Control name="comment" type="text" placeholder={log.comment} />
          </Form.Group>
          <Form.Group controlId="created-at">
            <Form.Label>Created At</Form.Label>
            <Form.Control name="created_at" type="text" placeholder={formatDate(log.created_at)} />
          </Form.Group>

          <Form.Group controlId="old-ticket-values">
            <Form.Label>Old Ticket Values</Form.Label>
            <Form.Control as="textarea" rows={10} name="old_ticket_values" placeholder={log.old_ticket_value} />
          </Form.Group>

          <Form.Group controlId="changed-columns">
            <Form.Label>Changed Columns</Form.Label>
            <Form.Control as="textarea" rows={10} name="changed_columns" placeholder={log.changed_columns} />
          </Form.Group>
        </Col>
      </Row>
      <Row>
        <Col>
          <p style={{ marginTop: '10px' }}><Link to="/admin/activity">Go Back</Link></p>
        </Col>
      </Row>
    </Container>
  )
}

export default AdminActivitySingle
