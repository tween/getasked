import React, { Fragment, useEffect, useState } from "react";
import { Button, Form, Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import useHelper from "../../hooks/useHelper";
import { getTickets, getUsers } from "../../services/admin.service";


function AdminTickets() {
  const { formatDate } = useHelper();
  const [tickets, setTickets] = useState([])

  useEffect(async () => {
    let tickets = await getTickets()
    setTickets(tickets)
  }, [])

  const deleteStatus = [
    { id: 1, name: 'Active', is_deleted: 0 },
    { id: 2, name: 'Archived', is_deleted: 1 },
  ]

  return (
    <Fragment>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Title</th>
            <th>Content</th>
            <th>Assigend To</th>
            <th>Status</th>
            <th>Is Deleted</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {tickets && tickets.map(ticket => {
            return <tr key={ticket.id}>
              <td>{ticket.id}</td>
              <td>{ticket.title}</td>
              <td>{ticket.content.length > 200 ? ticket.content.slice(0, 200) + "..." : ticket.content}</td>
              <td>{ticket.assignee_email}</td>
              <td>{ticket.ticket_status_name}</td>
              <td>{deleteStatus.find(status => status.is_deleted === ticket.is_deleted).name}</td>
              <td>
                <Button variant="primary"><Link to={"/admin/tickets/edit/" + ticket.id} style={{ backgroundColor: "white" }}>Actions</Link></Button>
              </td>
            </tr>
          })}
        </tbody>
      </Table>
    </Fragment>
  )
}

export default AdminTickets;
