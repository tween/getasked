import React, { Fragment, useEffect, useState } from "react";
import { Table, Form } from "react-bootstrap";
import { getUsers } from "../../services/admin.service";
import InfoMessageHeader from "../InfoMessageHeader";
import { makeUserActive } from "../../services/admin.service";
import useHelper from "../../hooks/useHelper";

function AdminUsers() {
  const { formatDate } = useHelper();
  const [users, setUsers] = useState([])
  const [userStatus, setUserStatus] = useState(true);
  const [response, setResponse] = useState({
    success: '',
    error: ''
  })
  const [needUpdate, setNeedUpdate] = useState(false);
  useEffect(async () => {
    let users = await getUsers()
    setUsers(users)
  }, [needUpdate])

  async function handleChange(e, id) {
    let makeUserActiveResponse = await makeUserActive(id);
    if (makeUserActiveResponse.error) {
      setResponse(response => ({ ...response, error: makeUserActiveResponse.error.message }))
    } else {
      setResponse(response => ({ ...response, success: makeUserActiveResponse.success.message }))
    }
    setNeedUpdate(!needUpdate);
  }

  return (
    <Fragment>
      <InfoMessageHeader response={response} />
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Email</th>
            <th>Role</th>
            <th>Status</th>
            <th>Created At</th>
            <th>Updated At</th>
          </tr>
        </thead>
        <tbody>
          {users && users.map(user => {
            return <tr key={user.id}>
              <td>{user.id}</td>
              <td>{user.email}</td>
              <td>{user.role}</td>
              <td>
                <Form.Control as="select" name="user-status" onChange={(e) => handleChange(e, user.id)} defaultValue={user.status} readOnly={user.status === 'ACTIVE' ? true : false}>
                  <option selected={user.status === 'ACTIVE' ? true : false} defaultValue="ACTIVE">ACTIVE</option>
                  <option selected={user.status === 'PENDING' ? true : false} defaultValue="PENDING">PENDING</option>
                </Form.Control>
              </td>
              <td>{formatDate(user.created_at)}</td>
              <td>{formatDate(user.updated_at)}</td>
            </tr>
          })}
        </tbody>
      </Table>
    </Fragment>
  )
}

export default AdminUsers;
