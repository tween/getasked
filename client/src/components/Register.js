import React, { useState } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { register } from "../services/auth.service";
import "../styles/auth.css"
import InfoMessageHeader from "./InfoMessageHeader";
import RenderError from "./RenderError";


function Register() {
  const [inputs, setInputs] = useState({
    email: '',
    password: '',
    passwordConfirmation: ''
  });
  const [response, setResponse] = useState({
    success: '',
    error: ''
  });
  const [errors, setErrors] = useState({});

  const { email, password, passwordConfirmation } = inputs;

  function handleChange(e) {
    const { name, value } = e.target;
    setInputs(inputs => ({ ...inputs, [name]: value }));
  }

  function validate() {
    let error = false;
    let fields = Object.keys(inputs);
    fields.forEach(field => {
      let displayField = field.replaceAll('_', ' ');
      displayField = displayField.charAt(0).toUpperCase() + displayField.slice(1) + ' is required';
      if (inputs[field] === "") {
        setErrors(errors => ({ ...errors, [field]: displayField }))
      } else {
        setErrors(errors => ({ ...errors, [field]: '' }));
      }
      error = true;
    })
    if (password !== passwordConfirmation) setErrors(errors => ({ ...errors, passwordConfirmation: 'Passwords must match' }));
    return error;
  }

  async function handleRegisterForm(e) {
    e.preventDefault();
    const error = validate();
    if (!!error) {
      let registerResponse = await register(inputs);
      if (registerResponse.error) {
        setResponse(response => ({ ...response, error: registerResponse.error.message }))
      } else {
        setResponse(response => ({ ...response, success: registerResponse.success.message }))
      }
      Object.keys(inputs).map(key => {
        setInputs(inputs => ({ ...inputs, [key]: '' }));
      })
    }
  }

  return (
    <Container>
      <InfoMessageHeader response={response} />
      <Row>
        <Col className="form-header">
          Register
        </Col>
      </Row>
      <Row>
        <Col>
          <Form className="form">
            <Form.Group controlId="email">
              <Form.Label>Email address</Form.Label>
              <Form.Control name="email" value={email} type="email" onChange={handleChange} placeholder="Enter email" />
              <RenderError errors={errors} name="email" />
            </Form.Group>

            <Form.Group controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control name="password" value={password} type="password" onChange={handleChange} placeholder="Password" />
              <RenderError errors={errors} name="password" />
            </Form.Group>

            <Form.Group controlId="passwordConfirmation">
              <Form.Label>Password</Form.Label>
              <Form.Control name="passwordConfirmation" value={passwordConfirmation} type="password" onChange={handleChange} placeholder="Repeat password" />
              <RenderError errors={errors} name="passwordConfirmation" />
            </Form.Group>

            <Button variant="primary" onClick={handleRegisterForm}>
              Register
            </Button>
          </Form>
        </Col>
      </Row>
      <Row>
        <Col>
          <p style={{ marginTop: '10px' }}>You already have account. Go to <Link to="/login">Login</Link></p>
        </Col>
      </Row>
    </Container>
  );
}

export default Register;
