import React from 'react'
import { Row, Col } from 'react-bootstrap'

function InfoMessageHeader({ response }) {
  return (
    <Row>
      <Col>
        <p style={response.error ? { color: "red" } : { color: "green" }}>{response.success || response.error}</p>
      </Col>
    </Row>
  )
}

export default InfoMessageHeader
