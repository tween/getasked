import React, { Fragment } from 'react'
import { Form } from 'react-bootstrap'


function RenderError({ errors, name }) {
  return (
    <Fragment>
      {errors[name] &&
        <Form.Text className="form-error">
          {errors[name]}
        </Form.Text>
      }
    </Fragment>
  )
}

export default RenderError
