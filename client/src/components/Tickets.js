import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { getTickets } from "../services/ticket.service";
import SingleTicket from "./SingleTicket";


function Tickets() {
  const [tickets, setTickets] = useState([]);

  useEffect(async () => {
    let fetchedTickets = await getTickets('all');
    setTickets(fetchedTickets);
  }, [])

  return (
    <Container>
      <Row>
        {tickets && tickets.map(ticket => {
          return <Col key={ticket.id} md={4} style={{ padding: "20px 20px" }}><SingleTicket key={ticket.id} {...ticket} /></Col>
        })}
      </Row>
    </Container>
  )
}

export default Tickets;
