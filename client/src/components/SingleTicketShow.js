import React, { useState, useEffect } from 'react'
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import useHelper from '../hooks/useHelper';
import useUser from '../hooks/useUser';
import InfoMessageHeader from './InfoMessageHeader';
import { deleteTicket, getSingleTicket, getTicketStatus, updateTicket } from '../services/ticket.service';
import { getUsers } from '../services/user.service';
import { useHistory } from "react-router-dom";

function SingleTicketShow(props) {
  const history = useHistory();
  const [checkBoxActive, setCheckBoxActive] = useState(false);
  const [ticket, setTicket] = useState({});
  const [ticketStatus, setTicketStatus] = useState([]);
  const [users, setUsers] = useState([]);
  const [inputs, setInputs] = useState({
    title: '',
    content: '',
    ticket_status_id: '',
    assigned_to_id: ''
  });
  const [response, setResponse] = useState({
    success: '',
    error: ''
  });
  const [needUpdate, setNeedUpdate] = useState(false);
  const { id } = props.match.params;
  const { findChangedIndexesinObjects, formatDate } = useHelper();
  const { loggedUser } = useUser();

  function handleCheckBoxClick() {
    setCheckBoxActive((prevState) => {
      return !prevState;
    })
  }

  function handleChange(e) {
    const { name, value } = e.target;
    setInputs(inputs => ({ ...inputs, [name]: value }));
  }

  useEffect(async () => {
    let singleTicket = await getSingleTicket(id);
    setTicket(singleTicket);
    setInputs(inputs => ({ ...inputs, ...singleTicket }));
    let fetchedTicketStatus = await getTicketStatus();
    setTicketStatus(fetchedTicketStatus);
    let fetchedUsers = await getUsers();
    setUsers(fetchedUsers);
  }, [needUpdate])

  async function handleEditSingleTicket() {
    const { title, content, assigned_to_id, ticket_status_id } = inputs;
    let matchStatusFindId = ticket_status_id ? ticket_status_id : ticket.ticket_status_id
    const ticket_status_name = ticketStatus.find(status => status.id == matchStatusFindId).name;
    let matchUserFindId = assigned_to_id ? assigned_to_id : ticket.assigned_to_id;
    const assigned_to_email = users.find(user => user.id == matchUserFindId);
    let updateResponse = await updateTicket(id, { title, content, assigned_to_id, ticket_status_id, ticket_status_name, assignee_email: assigned_to_email.email });
    setCheckBoxActive((prevState) => {
      return !prevState;
    })
    if (updateResponse.error) {
      setResponse(response => ({ ...response, error: updateResponse.error.message }))
    } else {
      setResponse(response => ({ ...response, success: updateResponse.success.message }))
    }
    setNeedUpdate(!needUpdate);
  }

  async function handleDeleteTicket() {
    let deleteResponse = await deleteTicket(id);
    if (deleteResponse) {
      history.push('/tickets');
    }
  }

  return (
    <Container>
      <InfoMessageHeader response={response} />
      <Row>
        <Col className="form-header">
          Ticket Details
        </Col>
      </Row>
      <Row>
        <Col>
          <Form.Check
            type="checkbox"
            label="Enter Edit mode"
            checked={checkBoxActive}
            onChange={handleCheckBoxClick}
          />
          <fieldset disabled={!checkBoxActive}>
            <Form className="form">

              <Form.Group controlId="ticket-title">
                <Form.Label>Ticket title No. {id}</Form.Label>
                <Form.Control name="title" type="text" placeholder="Enter Ticket title" value={inputs.title} readOnly={!checkBoxActive} onChange={handleChange} />
              </Form.Group>
              <Form.Group controlId="ticket-content">
                <Form.Label>Ticket content</Form.Label>
                <Form.Control as="textarea" rows={5} name="content" value={inputs.content} readOnly={!checkBoxActive} onChange={handleChange} />
              </Form.Group>

              <Form.Group controlId="ticket-status">
                <Form.Label>Ticket Status</Form.Label>
                <Form.Control as="select" name="ticket_status_id" readOnly={!checkBoxActive} defaultValue={ticket.ticket_status_id} onChange={handleChange}>
                  {ticketStatus && ticketStatus.map(({ id, name }) => {
                    return (
                      <option selected={ticket.ticket_status_id === id ? true : false} value={id} key={id}>{name}</option>
                    )
                  })}
                </Form.Control>
              </Form.Group>

              <Form.Group controlId="ticket-assign-to">
                <Form.Label>Assign Ticket to</Form.Label>
                <Form.Control as="select" name="assigned_to_id" readOnly={!checkBoxActive} defaultValue={ticket.assigned_to_id} onChange={handleChange} >
                  <option selected={ticket.assigned_to_id === null ? true : false} value={null}>Not Assigned</option>
                  {users && users.map(({ id, email }) => {
                    return (
                      <option selected={ticket.assigned_to_id === id ? true : false} value={id} key={id}>{email}</option>
                    )
                  })}

                </Form.Control>
              </Form.Group>

              <Form.Row>
                <Form.Group as={Col} controlId="ticket-created-at">
                  <Form.Label>Created At</Form.Label>
                  <Form.Control placeholder={formatDate(ticket.created_at)} readOnly />
                </Form.Group>

                <Form.Group as={Col} controlId="ticket-updated-at">
                  <Form.Label>Updated At</Form.Label>
                  <Form.Control placeholder={formatDate(ticket.updated_at)} readOnly />
                </Form.Group>
              </Form.Row>
              <Form.Group controlId="ticket-creator">
                <Form.Label>Ticket created by</Form.Label>
                <Form.Control name="ticket-creator" placeholder={ticket.creator_email} readOnly />
              </Form.Group>
              {checkBoxActive &&
                <Row>
                  <Col>
                    <Button variant="primary" onClick={handleEditSingleTicket}>Edit Ticket</Button>
                  </Col>
                  <Col>
                    <Button variant="danger" onClick={handleDeleteTicket} disabled={ticket.creator_id !== loggedUser.id ? true : false} >Delete Ticket</Button>
                  </Col>
                </Row>
              }
            </Form>
          </fieldset>
        </Col>
      </Row>
    </Container >
  )
}

export default SingleTicketShow
