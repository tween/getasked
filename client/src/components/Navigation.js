import React, { useEffect, Fragment } from "react";
import {
  Navbar,
  Nav,
  NavDropdown,
  Form,
  FormControl,
  Button,
} from "react-bootstrap";
import {
  Link,
  useHistory,
} from "react-router-dom";
import useUser from "../hooks/useUser";
import { checkIfLoggedIn, logout } from "../services/auth.service";
import "../styles/navigation.css"



function Navigation() {
  const history = useHistory();
  const { loggedUser, setLoggedUser } = useUser();
  useEffect(() => {
    // checkIfLoggedIn(history);
  }, [])

  function handleLogout() {
    logout(history);
    setLoggedUser({})
  }

  return (
    <Navbar bg="light" expand="lg">
      <Link to="/"><Navbar.Brand>geTasked</Navbar.Brand></Link>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      {loggedUser.email ?
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Link className="navlink" to="/tickets">Tickets</Link>
            <Link className="navlink" to="/tickets/create">Create Ticket</Link>
            {loggedUser.role === "Admin" ?
              <Fragment>
                <Link className="navlink" to="/admin">Admin</Link>
                <Link className="navlink" to="/admin/tickets">Admin Tickets</Link>
                <Link className="navlink" to="/admin/users">Admin Users</Link>
                <Link className="navlink" to="/admin/activity">Admin Tickets Activity</Link>
              </Fragment>
              : null
            }
          </Nav>
          <NavDropdown title={loggedUser.email || 'User'} id="user-dropdown">
            <NavDropdown.Item>Profile</NavDropdown.Item>
            <NavDropdown.Item onClick={handleLogout}>Logout</NavDropdown.Item>
          </NavDropdown>
        </Navbar.Collapse> :
        null
      }
    </Navbar>
  );
}

export default Navigation;
