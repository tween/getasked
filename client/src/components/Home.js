import React, { useEffect, useRef } from "react";

function Home() {
  const firstEnter = useRef(true);
  useEffect(() => {
    if (!localStorage.getItem('firstLoad')) {
      localStorage['firstLoad'] = true;
      window.location.reload();
    }
    else {
      localStorage.removeItem('firstLoad');
    }
    return firstEnter.current = false;
  }, [])
  return <div style={{ textAlign: "center", fontSize: "40px" }}>Wellcome to App</div>;
}

export default Home;
