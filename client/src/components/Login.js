import React, { useState, useEffect, useRef } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import useAuth from "../hooks/useAuth";
import { login } from "../services/auth.service";
import "../styles/auth.css"
import { Link, useHistory } from "react-router-dom";
import RenderError from "./RenderError";
import InfoMessageHeader from "./InfoMessageHeader";
import useUser from "../hooks/useUser";


function Login() {
  const history = useHistory();
  // const firstEnter = useRef(true);
  // useEffect(() => {
  //   if (firstEnter.current) {
  //     window.location.href = "/login";
  //   }
  //   return firstEnter.current = false;
  // }, [])

  const [inputs, setInputs] = useState({
    email: '',
    password: ''
  });
  const [errors, setErrors] = useState({});
  const [response, setResponse] = useState({
    success: '',
    error: ''
  });
  const { email, password } = inputs;

  function handleChange(e) {
    const { name, value } = e.target;
    setInputs(inputs => ({ ...inputs, [name]: value }));
  }

  function validate() {
    let error = false;
    let fields = Object.keys(inputs);
    fields.forEach(field => {
      let displayField = field.replaceAll('_', ' ');
      displayField = displayField.charAt(0).toUpperCase() + displayField.slice(1) + ' is required';
      if (inputs[field] === "") {
        setErrors(errors => ({ ...errors, [field]: displayField }))
      } else {
        setErrors(errors => ({ ...errors, [field]: '' }));
      }
      error = true;
    })
    return error;
  }

  async function handleSubmit(e) {
    e.preventDefault();
    const error = validate();
    if (!!error) {
      let loginResponse = await login(inputs, history);

      if (loginResponse) {
        if (loginResponse.error) {
          setResponse(response => ({ ...response, error: loginResponse.error.message }))
        } else {
          setResponse(response => ({ ...response, success: loginResponse.success.message }))
        }
      } else {
        setResponse(response => ({ ...response, error: 'Something went wrong' }))
      }
    }
  }

  return (
    <Container>
      <InfoMessageHeader response={response} />
      <Row>
        <Col className="form-header">
          Login
        </Col>
      </Row>
      <Row>
        <Col>
          <Form className="form">
            <Form.Group controlId="email">
              <Form.Label>Email address</Form.Label>
              <Form.Control value={email} name="email" onChange={handleChange} type="email" placeholder="Enter email" />
              <RenderError errors={errors} name="email" />
            </Form.Group>
            <Form.Group controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control value={password} name="password" onChange={handleChange} type="password" placeholder="Password" />
              <RenderError errors={errors} name="password" />
            </Form.Group>
            <Button variant="primary" onClick={handleSubmit}>
              Login
            </Button>
          </Form>
        </Col>
      </Row>
      <Row>
        <Col>
          <p style={{ marginTop: '10px' }}>You don't have account. Go to <Link to="/register">Register</Link></p>
        </Col>
      </Row>
    </Container>
  )
}

export default Login;
