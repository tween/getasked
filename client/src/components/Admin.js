import React, { Fragment, useState, useEffect } from "react";
import { Form, Table, Row, Col, Button, Container } from 'react-bootstrap';
import useHelper from "../hooks/useHelper";
import { createTicketStatus, getAdminTicketStatus, deleteTicketStatus, updateTicketStatus } from "../services/admin.service";
import InfoMessageHeader from "./InfoMessageHeader";


function Admin() {
  const [response, setResponse] = useState({
    success: '',
    error: ''
  })
  const [ticketStatus, setTicketStatus] = useState([]);
  const { formatDate } = useHelper();
  const [statusName, setStatusName] = useState('');
  const [needUpdate, setNeedUpdate] = useState(false);
  const [updateName, setUpdateName] = useState('');
  const [inputs, setInputs] = useState({});

  useEffect(async () => {
    let fetchedTicketStatus = await getAdminTicketStatus();
    setTicketStatus(fetchedTicketStatus);
  }, [needUpdate])

  async function handleCreateTicketStatus() {
    let createStatusNameResponse = await createTicketStatus({ name: statusName });
    if (createStatusNameResponse.error) {
      setResponse(response => ({ ...response, error: createStatusNameResponse.error.message }))
    } else {
      setResponse(response => ({ ...response, success: createStatusNameResponse.success.message }))
    }
    setNeedUpdate(!needUpdate);
  }

  function handleChange(e) {
    setStatusName(e.target.value);
  }

  function handleEditChange(e) {

  }

  async function handleUpdateTicketStatus(id) {
    let updateTicketStatusResponse = await updateTicketStatus();
    if (updateTicketStatusResponse.error) {
      setResponse(response => ({ ...response, error: updateTicketStatusResponse.error.message }))
    } else {
      setResponse(response => ({ ...response, success: updateTicketStatusResponse.success.message }))
    }
    setNeedUpdate(!needUpdate);
  }

  async function handleTicketStatusDelete(id) {
    let ticketStatusDeleteResponse = await deleteTicketStatus(id);
    if (ticketStatusDeleteResponse.error) {
      setResponse(response => ({ ...response, error: ticketStatusDeleteResponse.error.message }))
    } else {
      setResponse(response => ({ ...response, success: ticketStatusDeleteResponse.success.message }))
    }
    setNeedUpdate(!needUpdate);
  }

  return (
    <Fragment>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Creator</th>
            <th>Created At</th>
            <th>Updated At</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {ticketStatus && ticketStatus.map(ticket => {
            return <tr key={ticket.id}>
              <td>{ticket.id}</td>
              <td>
                <Form.Group controlId="ticket-status-name">
                  <Form.Control name="name" value={ticket.name} type="text" onChange={handleEditChange} placeholder="Ticket Status Name" />
                </Form.Group>
              </td>
              <td>{ticket.creator_email}</td>
              <td>{formatDate(ticket.created_at)}</td>
              <td>{formatDate(ticket.updated_at)}</td>
              <td>
                <Button variant="info" onClick={() => handleUpdateTicketStatus(ticket.id)}>EDIT</Button>
                <Button variant="danger" onClick={() => handleTicketStatusDelete(ticket.id)}>DELETE</Button>
              </td>
            </tr>
          })}
        </tbody>
      </Table>
      <Container>
        <InfoMessageHeader response={response} />
        <Row>
          <Col className="form-header">
            Create Ticket Status
        </Col>
        </Row>
        <Row>
          <Col>
            <Form className="form">
              <Form.Group controlId="ticket-status-name">
                <Form.Label>Ticket Status Name</Form.Label>
                <Form.Control name="name" value={statusName} type="text" onChange={handleChange} placeholder="Ticket Status Name" />
              </Form.Group>
              <Button variant="primary" onClick={handleCreateTicketStatus}>
                Create Ticket Status
            </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    </Fragment>
  )
}

export default Admin;
