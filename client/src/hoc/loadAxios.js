import React, { Fragment } from "react";
import useAxios from "../hooks/useAxios";

const LoadAxios = ({ children }) => {
  useAxios();

  return (
    <Fragment>
      {children}
    </Fragment>
  )
};

export default LoadAxios;
