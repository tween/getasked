import React, { useState } from 'react'
import Navigation from "./components/Navigation";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  BrowserRouter as Router,
  withRouter,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import Home from "./components/Home";
import Tickets from "./components/Tickets";
import Admin from "./components/Admin";
import Login from './components/Login';
import Register from './components/Register';
import CreateTicket from './components/CreateTicket';
import SingleTicketShow from './components/SingleTicketShow';
import AdminActivity from './components/admin/AdminActivity';
import AdminTickets from './components/admin/AdminTickets';
import AdminUsers from './components/admin/AdminUsers';
import useUser from './hooks/useUser';
import AdminTicketsSingle from './components/admin/AdminTicketsSingle';
import AdminActivitySingle from './components/admin/AdminActivitySingle';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    localStorage.getItem('token')
      ? <Component {...props} />
      : <Redirect to="/login" />
  )} />
)

const AdminRoute = ({ component: Component, loggedUser, ...rest }) => (
  <Route
    {...rest}
    render={
      (props) => (
        loggedUser.role === "Admin"
          ? <Component {...props} />
          : <Redirect to="/" />
      )
    }
  />
);

function Main() {
  const { loggedUser } = useUser();
  return (
    <div>
      <Switch>
        <PrivateRoute path="/" component={Home} exact={true} />
        <AdminRoute path="/admin" component={Admin} loggedUser={loggedUser} exact={true} />
        <AdminRoute path="/admin/users" component={AdminUsers} loggedUser={loggedUser} exact={true} />
        <AdminRoute path="/admin/tickets" component={AdminTickets} loggedUser={loggedUser} exact={true} />
        <AdminRoute path="/admin/tickets/edit/:id" component={AdminTicketsSingle} loggedUser={loggedUser} exact={true} />
        <AdminRoute path="/admin/activity" component={AdminActivity} loggedUser={loggedUser} exact={true} />
        <AdminRoute path="/admin/activity/show/:id" component={AdminActivitySingle} loggedUser={loggedUser} exact={true} />
        <Route path="/login" component={Login} exact={true} />
        <Route path="/register" component={Register} exact={true} />
        <Route path="/tickets" component={Tickets} exact={true} />
        <Route path="/tickets/show/:id" component={SingleTicketShow} exact={true} />
        <PrivateRoute path="/tickets/create" component={CreateTicket} exact={true} />
        {/* <PrivateRoute path="/user" component={User} exact={true} /> */}
        <Route path='*' exact={true} component={Home} />
      </Switch>
    </div>
  )
}

export default Main;
