import { useState, useEffect } from 'react';

function useAuth() {
  const [isLoggedIn, setIsLoggedIn] = useState(true);

  const updateLogin = (status) => {
    setIsLoggedIn(status)
  }

  return { isLoggedIn, setIsLoggedIn, updateLogin };
}

export default useAuth;