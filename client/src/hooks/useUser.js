import { useState, useEffect } from 'react';
import { getUserData } from '../services/auth.service';


function useUser() {
  const [loggedUser, setLoggedUser] = useState({});

  useEffect(async () => {
    let fetchedUser = await getUserData();
    setLoggedUser((prevUser) => {
      return fetchedUser
    });
  }, [])

  return { loggedUser, setLoggedUser };
}

export default useUser;