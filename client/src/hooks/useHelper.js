function useHelper() {
  const findChangedIndexesinObjects = (primeObject, compareObject) => {
    let changedValues = [];
    Object.keys(primeObject).forEach(key => {
      console.log(primeObject[key], compareObject[key]);
      if (primeObject[key] !== compareObject[key]) {
        changedValues.push(key);
      }
    })
    return changedValues;
  }

  const formatDate = (inputTime) => {
    let date = new Date(inputTime);
    let dateString =
      ("0" + date.getDate()).slice(-2) +
      "-" +
      ("0" + (date.getMonth() + 1)).slice(-2) +
      "-" +
      date.getFullYear() +
      " " +
      ("0" + date.getHours()).slice(-2) +
      ":" +
      ("0" + date.getMinutes()).slice(-2);
    return dateString;
  }

  return { findChangedIndexesinObjects, formatDate }
}

export default useHelper;
