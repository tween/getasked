import axios from "axios";
import { useHistory } from "react-router-dom";

function useAxios() {
  const history = useHistory();
  const token = localStorage.getItem('token');
  axios.interceptors.response.use(req => {
    if (req.status === 401) history.push('/login')
    return req;
  });

  axios.defaults.headers = {
    "Content-Type": "application/json",
    ...(token && { "authorization": token })
  };

  // axios.defaults.baseURL = `${process.env.REACT_APP_API_URL}/api/`;
}

export default useAxios;