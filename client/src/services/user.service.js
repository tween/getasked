import axios from "axios";

export const getUsers = () => {
  return axios({
    method: 'GET',
    url: '/api/users'
  })
    .then(response => {
      return response.data;
    })
    .catch(error => console.log('get users error ', error));
}