
import axios from "axios";

export const getUserData = () => {
  return axios({
    method: 'GET',
    url: '/api/me'
  })
    .then(response => {
      if (response.status === 200) {
        return response.data;
      }
    })
    .catch(error => {
      return true;
    })
}

export const checkIfLoggedIn = (history) => {
  return axios({
    method: 'GET',
    url: '/api/me'
  })
    .then(response => {
      if (response.status === 200) {
        history.push('/')
      } else {
        localStorage.removeItem('token')
        history.push('/login')
      }
    })
    .catch(error => {
      localStorage.removeItem('token')
      history.push('/login')
    })
}

export const checkIfLoggedIn1 = (history) => {
  let token = localStorage.getItem('token');
  if (token) {
    fetch("/api/me", {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token,
      },
    }).then(response => {
      if (response.status === 200) {
        history.push('/')
      } else {
        localStorage.removeItem('token')
        history.push('/login')
      }
    })
  } else {
    localStorage.removeItem('token')
    if (history && history.location.pathname !== '/login') {
      history.push('/login')
    }
  }
}

export const login = (data, history) => {
  return axios({
    method: 'POST',
    url: '/api/auth/login',
    data
  })
    .then(response => {
      if (response.data.token) {
        localStorage.setItem("token", response.data.token)
        history.push('/')
        return response.data;
      } else {
        localStorage.removeItem('token')
        return response.data;
      }
    })
    .catch(error => console.log('LOGIN ERROR ', error))
}

export const register = (data) => {
  return axios({
    method: 'POST',
    url: '/api/auth/register',
    data
  })
    .then(response => {
      return response.data;
    })
    .catch(error => console.log('REGISTER ERROR ', error))
}

export const logout = (history) => {
  localStorage.clear();
  history.push('/login');
}