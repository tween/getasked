import axios from 'axios';

export const getActivityLogs = () => {
  return axios({
    method: 'GET',
    url: '/api/admin/activity-logs'
  })
    .then(response => {
      return response.data;
    })
    .catch(error => console.log('Get activity logs ERROR ', error))
}

export const getSingleActivityLog = (id) => {
  return axios({
    method: 'GET',
    url: '/api/admin/activity-logs/single-log/' + id
  })
    .then(response => {
      return response.data;
    })
    .catch(error => console.log('Get activity logs ERROR ', error))
}