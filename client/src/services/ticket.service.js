import axios from 'axios';

export const getTicketStatus = () => {
  return axios({
    method: 'GET',
    url: '/api/tickets/ticket-status'
  })
    .then(response => {
      return response.data;
    })
    .catch(error => {
      console.log('Error getTicketStatus ', error)
      return error;
    })
}

export const createTicket = (data) => {
  return axios({
    method: 'POST',
    url: '/api/tickets/create-ticket',
    data
  })
    .then(response => {
      return response.data;
    })
    .catch(error => {
      console.log('Error create ticket ', error)
      return error;
    })
}

export const getSingleTicket = (id) => {
  return axios({
    method: 'GET',
    url: '/api/tickets/show/' + id,
  })
    .then(response => {
      return response.data;
    })
    .catch(error => {
      console.log('Error single ticket ', error)
      return error;
    })
}

export const updateTicket = (id, data) => {
  return axios({
    method: 'PUT',
    url: '/api/tickets/edit/' + id,
    data
  })
    .then(response => {
      return response.data;
    })
    .catch(error => {
      console.log('Error update ticket ', error)
      return error;
    })
}

export const deleteTicket = (id) => {
  return axios({
    method: 'DELETE',
    url: '/api/tickets/delete/' + id
  })
    .then(response => {
      return response.data;
    })
    .catch(error => {
      console.log('Error delete ticket ', error)
      return error;
    })
}

export const getTickets = (status) => {
  return axios({
    method: 'GET',
    url: '/api/tickets/status/' + status
  })
    .then(response => {
      return response.data;
    })
    .catch(error => {
      console.log('Error get tickets ', error)
      return error;
    })
}