import axios from 'axios';

export const getUsers = () => {
  return axios({
    method: 'GET',
    url: '/api/admin/users'
  })
    .then(response => {
      return response.data;
    })
    .catch(error => console.log('getUsers error ', error))
}

export const getTickets = () => {
  return axios({
    method: 'GET',
    url: '/api/admin/tickets'
  })
    .then(response => {
      return response.data;
    })
    .catch(error => console.log('getTickets ERROR ', error))
}

export const getSingleTicket = (id) => {
  return axios({
    method: 'GET',
    url: '/api/admin/tickets/single-ticket/' + id
  })
    .then(response => {
      return response.data;
    })
    .catch(error => console.log('Get Admin Single Ticket ERROR ', error))
}

export const updateAdminTicket = (id, data) => {
  return axios({
    method: 'PUT',
    url: '/api/admin/tickets/' + id,
    data
  })
    .then(response => {
      return response.data;
    })
    .catch(error => {
      console.log('Error admin update ticket ', error)
      return error;
    })
}

export const hardDeleteTicket = (id) => {
  return axios({
    method: 'DELETE',
    url: '/api/admin/tickets/hard-delete/' + id
  })
    .then(response => {
      return response.data;
    })
    .catch(error => console.log('Hard Delete ERROR ', error))
}

export const softDeleteTicket = (id) => {
  return axios({
    method: 'DELETE',
    url: '/api/admin/tickets/soft-delete/' + id
  })
    .then(response => {
      return response.data;
    })
    .catch(error => console.log('Soft Delete ERROR ', error))
}

export const makeTicketActive = (id) => {
  return axios({
    method: 'PUT',
    url: '/api/admin/tickets/activate/' + id
  })
    .then(response => {
      return response.data;
    })
    .catch(error => console.log('Make Ticket Active ERROR ', error))
}

export const getAdminTicketStatus = () => {
  return axios({
    method: 'GET',
    url: '/api/admin/tickets/ticket-status'
  })
    .then(response => {
      return response.data;
    })
    .catch(error => console.log('Get admin Ticket Status ERROR ', error))
}

export const createTicketStatus = (data) => {
  return axios({
    method: 'POST',
    url: '/api/admin/tickets/create-status',
    data
  })
    .then(response => {
      return response.data;
    })
    .catch(error => console.log('Create ticket status ERROR ', error))
}

export const deleteTicketStatus = (id) => {
  return axios({
    method: 'DELETE',
    url: '/api/admin/tickets/delete/ticket-status/' + id
  })
    .then(response => {
      return response.data;
    })
    .catch(error => console.log('Delete Ticket Status ERROR ', error))
}

export const updateTicketStatus = (id, data) => {
  return axios({
    method: 'PUT',
    url: '/api/admin/tickets/update-status/' + id,
    data
  })
    .then(response => {
      return response.data;
    })
    .catch(error => console.log('Update Ticket Status ERROR ', error))
}

export const makeUserActive = (id) => {
  return axios({
    method: 'PUT',
    url: '/api/admin/users/' + id
  })
    .then(response => {
      return response.data;
    })
    .catch(error => console.log('Hard Delete ERROR ', error))
}